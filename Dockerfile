FROM centos:7

MAINTAINER Pedro Miguel Blanco <pmblanco@ardemans.com>

# Añadimos repositorios 
COPY ./lib/yum.repos.d/* /etc/yum.repos.d/

# Instalamos paquetes necesarios para trabajar con Jenkins
RUN yum install -y java openssl git224 && \
    yum clean all

# Creamos usuario para construcción
RUN groupadd -g 1001 builder && \
    useradd -u 1001 -g 1001 -G users -s /sbin/nologin -c "Builder User" builder

# Instalación de Helm
RUN curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash

# Script que arranca el esclavo de Jenkins y se conecta al Master
ADD lib/jenkins-slave /usr/local/bin/jenkins-slave

# Espacio de trabajo de Jenkins
RUN mkdir -m 777 -p /jenkins

WORKDIR /jenkins

ENTRYPOINT jenkins-slave

USER builder
